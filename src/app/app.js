	//Datos de mi API proporcionados por Open Weather Map
	const api = {
	key: '828ea15e4a65b0575a8afc4037bab17f',
	base: 'https://api.openweathermap.org/data/2.5/'
};


	// Uso el Keycode 13 que es la tecla ENTER para que comience la consulta en la API con el valor introducido en el Input
	const searchbox = document.querySelector('.search-box');
	searchbox.addEventListener('keypress', setQuery);

	function setQuery(evt) {
	if (evt.keyCode == 13) {
		getResults(searchbox.value);
	}
}
	//Se utiliza el fecth introduciendo las instrucciones dadas por la API, señalando que buscamos el tiempo que hace y le indicamos que la unidad utilizada será la métrica.
	function getResults(query) {
	fetch(`${api.base}weather?q=${query}&units=metric&lang=sp&APPID=${api.key}`)
		.then((weather) => {
			return weather.json();
		})
		.then(displayResults);
}
	//Aquí se coge el resultado otorgado por el query y el displayResults y se hace una consulta a la API, weather: información del tiempo, name el nombre de la ciudad, sys.country es para indicar el código del país.
	function displayResults(weather) {
	let city = document.querySelector('.location .city');
	city.innerText = `${weather.name}, ${weather.sys.country}`;

	//Aquí obtenemos la temperatura haciendo una consulta a la API, main.temp nos otorga los grados de forma determinada en imperial, pero nosotros le marcamos antes que lo entregue en unidad métrica, usamos el math.round para que nos de un valor entero.
	let temp = document.querySelector('.current .temp');
	temp.innerHTML = `${Math.round(weather.main.temp)}<span>°c</span>`;

	//Con el Weather.main es con lo que obtenemos si el tiempo que hace es soleado, lluvioso, nublado, nevado, etc... por defecto aparece en inglés.
	let weather_el = document.querySelector('.current .weather');
	weather_el.innerText = weather.weather[0].main;

  //Con este dato obtenemos la mínima temperatura y la máxima aproximada en el momento, no durante el día.
	let hilow = document.querySelector('.hi-low');
	hilow.innerText = `${Math.round(weather.main.temp_min)}°c / ${Math.round(weather.main.temp_max)}°c`;
}

