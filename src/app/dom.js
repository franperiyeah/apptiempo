//Crear elementos HTML
/*
<div id="top-nav">
    <nav>
        <a class="enlaces" href="#div1">Inicio</a>
        <a class="enlaces" href="#nosotros">Nosotros</a>
        <a class="enlaces" href="#Información">Información</a>
        <a class="enlaces" href="#contacto">Contacto</a>

    </nav>
  </div>
*/







const div1 = document.createElement('div');
  div1.className = "app-wrap";
  div1.id = "div1";
const header = document.createElement('header');
  header.id = "header";
  const topnav = document.createElement('div');
topnav.id = "top-nav";
const nav = document.createElement('nav');
nav.id = "nav"
const a1 = document.createElement('a');
a1.className = "enlaces";
a1.innerText = "Home";
a1.setAttribute("href", "#div1");
const a2 = document.createElement('a');
a2.className = "enlaces";
a2.innerText = "About"
a2.setAttribute("href", "#about");
const a3 = document.createElement('a');
a3.className = "enlaces";
a3.innerText = "Developer";
a3.setAttribute("href", "#developer");
const a4 = document.createElement('a');
a4.className = "enlaces";
a4.innerText = "Contact";
a4.setAttribute("href", "#contact");
const input = document.createElement('input');
  input.id = "input";
  input.className = "search-box";
    input.setAttribute("type", "text");
    input.setAttribute("autocomplete", "off");
    input.setAttribute("placeholder", "Enter city name");
const main = document.createElement('main');
  main.id = "main";
const section = document.createElement('section');
  section.id = "section";
  section.className = "location";
const div2 = document.createElement('div');
  div2.id = "div2";
  div2.className = "city";
  div2.innerText = "-----"
const div3 = document.createElement('div');
  div3.id = "div3";
  div3.className = "date";
const div4 = document.createElement('div');
  div4.id = "div4";
  div4.className = "current";
const div5 = document.createElement('div');
  div5.id = "div5";
  div5.className = "temp";
const span = document.createElement('span');
  span.id = "span";
  span.innerText = "°c"
const div6 = document.createElement('div');
  div6.id = "div6";
  div6.className = "weather";
  div6.innerText = "--------"
const div7 = document.createElement('div');
  div7.id = "div7";
  div7.className = "hi-low";
  div7.innerText = "**°c / **°c";
const divinicio = document.createElement('div');
  divinicio.id = "inicio";
  divinicio.className = "inicio";
const divabout = document.createElement('div');
  divabout.id = "about";
  divabout.className = "about";
const h1 = document.createElement('h1');
  h1.innerText = "Simple and efficient";
const p1 = document.createElement('p');
  p1.className = "p1";
  p1.innerText = "Because sometimes you just want to know the weather, simple and pleasant."
const divdeveloper = document.createElement('div');
  divdeveloper.id = "developer";
  divdeveloper.className = "developer";
const h2 = document.createElement('h2');
  h2.className = "h2";
  h2.innerText = "Francisco José Ruiz Santaclara";
const p2 = document.createElement('p');
  p2.className = "p2";
  p2.innerText = "Fullstack developer from the province of Cádiz, lover of minimalist designs and perfectionist with his work."
const img1 = document.createElement('img');
img1.className = "fototext";
img1.id = "foto";
img1.setAttribute("src", "imgs/fotoperfil.png")
const contact = document.createElement('section');
contact.id = "contact";
const boton = document.createElement('a');
  boton.className = "button-slide";
  boton.id = "boton";
  boton.innerText = "Talk To Me 👌"
  boton.setAttribute("data-hover", "fran_gear@hotmail.com");
  boton.setAttribute("href", "mailto:fran_gear@hotmail.com"); 

 
//-------------------------------------------------------------------------------------------------------

document.body.appendChild(div1);
document.getElementById("div1").appendChild(header);
document.getElementById("header").appendChild(topnav);
document.getElementById("top-nav").appendChild(nav);
document.getElementById("nav").appendChild(a1);
document.getElementById("nav").appendChild(a2);
document.getElementById("nav").appendChild(a3);
document.getElementById("nav").appendChild(a4);
document.getElementById("div1").appendChild(main);
document.getElementById("main").appendChild(input);
document.getElementById("main").appendChild(section);

document.getElementById("section").appendChild(div2);
document.getElementById("section").appendChild(div3);
document.getElementById("main").appendChild(div4);
document.getElementById("div4").appendChild(div5);
document.getElementById("div5").appendChild(span);
document.getElementById("div4").appendChild(div6);
document.getElementById("div4").appendChild(div7);
document.body.appendChild(divinicio);
document.body.appendChild(divabout);
document.getElementById("about").appendChild(h1);
document.getElementById("about").appendChild(p1);
document.body.appendChild(divdeveloper);
document.getElementById("developer").appendChild(h2);
document.getElementById("developer").appendChild(p2);
document.getElementById("developer").appendChild(img1);
document.body.appendChild(contact);
document.getElementById("contact").appendChild(boton);








//------------------------------------------------------------------------------------
/*
<div class="titulo">
    <h2>Descubre el tiempo de tu ciudad</h2>
</div>
<div class="logo">
    <a href="index.html"><img src="logo.png" width="150" height="128"></a>
</div>
    
  const divmaster = document.createElement('div')
  divmaster.id = "divmaster";
  divmaster.className = "app-wrap";
  const titulo = document.createElement('div');
  titulo.id = "titulo";
  titulo.className = "titulo";
  const texto = document.createElement('h2');
  h2.id = "h2";
  texto.innerText = "Descubre el tiempo de tu ciudad";
  const logo = document.createElement('div');
  logo.id = "logo";
  logo.className = "logo";
  const logolink = document.createElement('img');
  logolink.id = "logolink";
  logolink.setAttribute("src", "logo.png");


  document.body.appendChild(divmaster);
  document.getElementById("divmaster")appendChild(titulo);
  document.getElementById("titulo")appendChild(texto);
  document.getElementById("divmaster")appendChild(logo);
  document.getElementById("logo")appendChild(logolink);
  
*/